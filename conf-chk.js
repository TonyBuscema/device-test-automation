// initialize global variables
global.CONFIG = require('./lib/config');

exports.config = {
	beforeLaunch: () => {
		const dev_webdriver = require("./lib/device_webdriver");
		dev_webdriver.webdriver_start();
	},
	capabilities: {
		browserName: 'chrome',
		acceptSslCerts: true
	},
  seleniumAddress: 'http://' + CONFIG.device.ipaddr + ':4444/wd/hub',
  specs: ['specs/todo-spec.js']
};

