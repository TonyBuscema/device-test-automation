const exec = require("child_process").exec;
global.CONFIG = require('./config');

var check_cmd = `ssh -i ${CONFIG.device.keypath}${CONFIG.device.keyname} ${CONFIG.device.user}@${CONFIG.device.ipaddr} \"fuser 4444/tcp\"`; 
var start_cmd = `ssh -i ${CONFIG.device.keypath}${CONFIG.device.keyname} ${CONFIG.device.user}@${CONFIG.device.ipaddr} \"DISPLAY=:0 nohup gnome-terminal -e \'${CONFIG.webdriver.webdriver_path}/webdriver-manager start --versions.chrome=${CONFIG.webdriver.chromedriver_ver}\'\"`;
var stop_cmd = `ssh -i ${CONFIG.device.keypath}${CONFIG.device.keyname} ${CONFIG.device.user}@${CONFIG.device.ipaddr} \"fuser -k 4444/tcp\"`;

function webdriver_start() {
	webdriver_check(check_cmd, function (err, res) {
		console.log("Checking for running webdriver...");
		if (err) {
			console.log("Error:" + err);
		} else {
			if (res == null) {
				console.log("Webdriver not running, starting now...");
				exec(start_cmd, function (err, stdout, stderr) {
	    		if (err) {
						console.log(`error: ${err.message}`);
						return;
					} else {
						setTimeout(function() {
						webdriver_check(check_cmd, function (err, res) {
							if (err) {
								console.log("Error:" + err);
							} else {
								console.log("Webdriver started with pid" + res);
								if (res == null) {
									console.log("Webdriver not started");
								}
							}
						});
						}, 5000);
					}
				});
			} else {
				console.log("Webdriver already running with pid" + res);
			}
		}
	});
}

function webdriver_stop() {
	exec(stop_cmd);
}

function webdriver_check(command, callback) {
	exec(command, function (err, stdout, stderr) {
    if (err) {
			callback(stderr, null);
    } else {
			callback(null, stdout);
		}
	});
}

webdriver_start();
module.exports = { webdriver_start, webdriver_stop, webdriver_check, check_cmd, stop_cmd};
