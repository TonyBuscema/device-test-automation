const Dashboard = function () {
	this.wakeupScreen = $('avz-wake-up-screen[class="ng-scope ng-isolate-scope"]');
	this.dashboardMenu = $('a[data-activates="dropdown3"]');
	this.optionSettings = element(by.cssContainingText('a[class="gray-dark-text"]', 'Settings'));
	this.optionRestart = $('a[data-target="modal-restart"]');
	this.optionTurnoff = $('a[data-target="modal-turnoff"]');
};

module.exports = Dashboard;
