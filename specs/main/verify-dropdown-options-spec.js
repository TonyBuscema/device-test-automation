const dashboard = new (require('../../lib/page-objects/dashboard-page-objects'))();

describe('Verify Main Dropdown Options', function() {
  it('Verify dropdown options', function() {
		// Load apgar UI
    browser.get('https://localhost:1440');
		browser.waitForAngularEnabled(false);
 	  //expect(completedAmount.count()).toEqual(2);
		browser.sleep(5000);

		// Wait for wakeup screen animation to complete
		var EC = protractor.ExpectedConditions;
		browser.wait(EC.not(EC.visibilityOf(dashboard.wakeupScreen)), 10000);

		// Select ellipsis dropdown menu
		browser.wait(EC.visibilityOf(dashboard.dashboardMenu), 10000);
		dashboard.dashboardMenu.click();
		browser.sleep(5000);
	});

	it('Second test', function() {
		// Verify ellipsis dropdown items
		expect(dashboard.optionSettings.getText()).toEqual('Settings');
		expect(dashboard.optionRestart.getText()).toEqual('Restart');
		child = dashboard.optionTurnoff.element(by.className("material-icons right gray-light-text power ng-scope"));
		//buttonText = dashboard.optionTurnoff.getText().replace(child.getText(), '');
		childText = child.getText();
		console.log(childText);
		//expect(dashboard.optionTurnoff.getText()).toEqual('Turn Off');
		//expect(buttonText).toEqual('Turn Off');
	});
});
