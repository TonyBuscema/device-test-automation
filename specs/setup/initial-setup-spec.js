describe('Device Test Automation', function() {
  it('Get webpage URL', function() {
    browser.get('https://localhost:1440');
		var currentUrl;
		browser.waitForAngularEnabled(false);
		browser.getCurrentUrl().then(function (url) {
			currentUrl = url;
			console.log(currentUrl);
		}).then(function() {
			browser.wait(function() {
				return browser.getCurrentUrl().then(function (url) {
					return url !== currentUrl;
				});
			});
		}).then(function () {
			browser.getCurrentUrl().then(url => {
				console.log("URL is: "+ url);
			});
			browser.getTitle().then(title => {
				console.log("Title is: "+ title);
			});
 	   //expect(completedAmount.count()).toEqual(2);
		});
		var beginBtn = element(by.buttonText('Begin setup'));
		var EC = protractor.ExpectedConditions;
		browser.wait(EC.elementToBeClickable(beginBtn), 5000);
		beginBtn.click();
		var sysMenu = element(by.id('system-menu'));
		//browser.wait(EC.elementToBeClickable(sysMenu), 5000);
		sysMenu.click();
		browser.sleep(5000);
	});
});
