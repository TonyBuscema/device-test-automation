// initialize global variables
global.CONFIG = require('./lib/config');

exports.config = {
	capabilities: {
		browserName: 'chrome',
		chromeOptions: {
			args: [
				'--overscroll-history-navigation=0',
				'--disable-pinch',
				'--password-store=basic',
				'--allow-insecure-localhost',
				'--no-first-run',
				'--no-default-browser-check',
				'--touch-events=enabled',
				'--fast',
				'--fast-start',
				'--disable-popup-blocking',
				'--disable-infobars',
				'--disable-session-crashed-bubble',
				'--disable-tab-switcher',
				'--disable-translate',
				'--enable-low-res-tiling',
				'--autoplay-policy=no-user-gesture-required',
				'--kiosk',
				'--incognito'
			]
		}
	},
  seleniumAddress: 'http://' + CONFIG.device.ipaddr + ':4444/wd/hub',
	suites: {
		start: ['./specs/amwell-spec.js'],
		main: ['./specs/main/**/**-spec.js']
	},
  //specs: ['specs/main/verify-dropdown-options-spec.js']
};
