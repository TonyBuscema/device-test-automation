# Carepoint Device Functional Tests #

The device-test-automation project runs automated functional tests on Amwell Carepoint devices.
The tests are run on a laptop or Linux server which connects to a Selenium server on the device.

## _Quickstart Procedure_ ##
### Setup ###

1. Prerequisites
	* Device should be on the same LAN as the machine running the tests (laptop, server, etc.)
	* Add device to Enterprise
	* Put device in development mode by running the entire procedure from the below link.  Once this step is done it should only need to be repeated after an OS update:
      https://emergemd.jira.com/wiki/spaces/HW/pages/561283116/Get+a+device+into+Development+Mode
2. Clone device-test-automation repo on machine which will run the automated tests (laptop, Linux server, etc.)
3. Install protractor in cloned repo directory (<path_to_repo>/device-test-automation):

	`yarn install`
	
	
4. In the ./lib/config.js file replace the REPLACE_ME string with the IP address of your Amwell device.
	
### Running Tests ###

1. Run setup script to install protractor and start the webdriver on the device:

	`yarn run device-setup`

2. Run the startup script:

	`yarn run device-test`
	
	This will run tests defined in the conf.js file.  Tests need to be added in spec files in the ./specs directory