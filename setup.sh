############################################################################
## 
##  setup.sh
##  This script configures an Amwell Carepoint device for automated testing.
##  It creates an ssh keypair and pushes the public key to the device,
##  installs default-jre, and installs and configures protractor, selenium,
##  and webdriver-manager.
##  Before running setup, device must be in dev mode with ssh enabled.
##
##  Usage: 
##  		From repo base directory (device-test-automation/), run:
##
##  		yarn run device-setup
##
############################################################################

keyname=`cat ./lib/config.js | grep keyname | awk -F'"' '{print $4}'`
ipaddr=`cat ./lib/config.js | grep ipaddr | awk -F'"' '{print $4}'`
chromedriver_ver=`cat ./lib/config.js | grep chromedriver | awk -F'"' '{print $4}'`

if [ $ipaddr == "REPLACE_ME" ]; then
	echo "Need to enter device IP address in ./lib/config.js";
	exit;
fi;

ssh-keygen -t rsa -f ./$keyname -C kiosk@amwell -N ""
keystr=$(<$keyname.pub)

ssh -t amwell@$ipaddr "\

	## Install default-jre if not already installed ##
	dpkg -s default-jre > /dev/null 2>&1; \
	if [ \$? == 1 ]; then \
		sudo apt update; \
		sudo apt install default-jre -y; else \
		echo \"default-jre is already installed\"; \
	fi; \

	## Push kiosk user's public key to device ##
	sudo -S mkdir -m700 -p /home/kiosk/.ssh; \
	echo $keystr | sudo tee /home/kiosk/.ssh/authorized_keys 1>/dev/null; \
	sudo chown -R kiosk:kiosk /home/kiosk/.ssh; \
	sudo chmod 600 /home/kiosk/.ssh/authorized_keys"

ssh -i ./device_rsa kiosk@$ipaddr "\

	## Install protractor if not already installed ##
	npm list protractor > /dev/null 2>&1; \
	if [ \$? == 1 ]; then \
		npm install protractor -y; else \
		echo \"protractor is already installed\"; \
	fi; \

	## Update webdriver with correct chromedriver version ##
	webdriver_path='./node_modules/protractor/node_modules/webdriver-manager'; \
	chromedriver_ver_curr=\`ls \$webdriver_path/selenium/chromedriver* | grep -v zip | awk -F'_' '{print \$NF}'\`; \
	if [ \$chromedriver_ver_curr == $chromedriver_ver ]; then \
		echo \"chromedriver version (\$chromedriver_ver_curr) is correct\"; else \
		\$webdriver_path/bin/webdriver-manager clean; \
		\$webdriver_path/bin/webdriver-manager update --versions.chrome=$chromedriver_ver; \
	fi;" 

node lib/device_webdriver.js
